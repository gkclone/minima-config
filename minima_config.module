<?php

/**
 * Implements hook_form_FORM_ID_alter() for system_theme_settings form.
 */
function minima_config_form_system_theme_settings_alter(&$form, &$form_state) {
  // Ensure we are on a theme's setting page, i.e. not the global settings page.
  if (empty($form['theme']['#value'])) {
    return;
  }

  $theme = $form['theme']['#value'];

  // Ensure this theme provides minima configuration.
  if (!minima_config_theme_is_configurable($theme)) {
    return;
  }

  // Get saved minima_config theme settings.
  $minima_config = theme_get_setting('minima_config', $theme);

  // Add fieldset for minima config fields.
  $form['minima_config'] = array(
    '#type' => 'fieldset',
    '#title' => t('Style settings'),
    '#weight' => -10,
    '#tree' => TRUE,
    'override' => array(
      '#type' => 'checkbox',
      '#title' => t('Override default settings'),
      '#default_value' => $minima_config['override'],
    ),
    'settings' => array(
      '#type' => 'item',
      '#states' => array(
        'invisible' => array(
          ':input[name="minima_config[override]"]' => array('checked' => FALSE),
        ),
      ),
    ),
  );

  // Generate form items for each setting defined by theme.
  foreach (minima_config_get_theme_settings($theme) as $setting_key => $setting) {
    if (array_key_exists('options', $setting) && is_array($setting['options'])) {
      // Build options list for setting.
      $options = array();

      foreach ($setting['options'] as $option_key => $option) {
        $label = '';

        // Use title for option label if one is provided.
        if (!empty($option['title'])) {
          $label .= $option['title'];
        }

        // Default label to option key if image or title not provided.
        if (empty($label)) {
          $label = $option_key;
        };

        $options[$option_key] = $label;
      }

      // Add select form item.
      $form['minima_config']['settings'][$setting_key] = array(
        '#type' => 'select',
        '#title' => $setting['title'],
        '#default_value' => $minima_config['settings'][$setting_key],
        '#options' => $options,
      );
    }
    elseif (array_key_exists('variable', $setting)) {
      // Add textfield form item.
      $form['minima_config']['settings'][$setting_key] = array(
        '#type' => 'textfield',
        '#title' => $setting['title'],
        '#default_value' => $minima_config['settings'][$setting_key],
      );
    }
  }

  // Add our submit handler before the default one.
  array_unshift($form['#submit'], 'minima_config_system_theme_settings_submit');
}

/**
 * Process system_theme_settings form submissions.
 */
function minima_config_system_theme_settings_submit($form, &$form_state) {
  $theme = $form_state['values']['theme'];
  $theme_settings = minima_config_get_theme_settings($theme);

  // Initialise less variables array if it doesn't exist.
  if (empty($form_state['values']['less'][$theme])) {
    $form_state['values']['less'][$theme] = array();
  }

  // Merge variables for selected options into less variables array.
  foreach ($form_state['values']['minima_config']['settings'] as $setting_key => $option) {
    if (array_key_exists('options', $theme_settings[$setting_key]) && is_array($theme_settings[$setting_key]['options'])) {
      $form_state['values']['less'][$theme] += $theme_settings[$setting_key]['options'][$option]['variables'];
    }
    elseif (array_key_exists('variable', $theme_settings[$setting_key])) {
      $form_state['values']['less'][$theme][$theme_settings[$setting_key]['variable']] = $option;
    }
  }
}

/**
 * Get settings for specified theme.
 */
function minima_config_get_theme_settings($theme) {
  $theme_settings = array();

  // Include theme's template.php so we can call functions defined by the theme.
  require_once drupal_get_path('theme', $theme) . '/template.php';

  // Call THEME_NAME_minima_config_settings function if it exists.
  $function = $theme . '_minima_config_settings';

  if (function_exists($function)) {
    $theme_settings = $function();
  }

  // Check the base theme.
  $themes = list_themes();

  if (isset($themes[$theme]->info['base theme'])) {
    $theme_settings = array_merge(minima_config_get_theme_settings($themes[$theme]->info['base theme']), $theme_settings);
  }

  return $theme_settings;
}

/**
 * Check if a theme provides minima configuration.
 *
 * @theme String
 *   The name of the theme to check.
 *
 * @return Bool
 */
function minima_config_theme_is_configurable($theme) {
  $theme_info = drupal_parse_info_file(drupal_get_path('theme', $theme) . '/' . $theme . '.info');
  return isset($theme_info['minima config']);
}
